package com.cts.skilltracker.ms_bf_skill_profile.clients;

import com.cts.skilltracker.ms_bf_skill_profile.models.SkillTrackerEvent;
import com.cts.skilltracker.ms_bf_skill_profile.models.TechSkills;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name="USER-SKILL",url="${userskill.url}")
public interface UserSkillClient {

    @GetMapping("/skill-tracker/skills/search-skill/skill/{criteria}")
    public ResponseEntity<List<SkillTrackerEvent>> searchUserBySkill(@PathVariable String criteria);

    @GetMapping("/skill-tracker/skills/search-skill/byAssociates")
    public ResponseEntity<List<SkillTrackerEvent>> searchUserByAssociates(@RequestParam List<String> associateList);
}
