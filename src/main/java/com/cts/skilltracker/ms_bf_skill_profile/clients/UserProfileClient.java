package com.cts.skilltracker.ms_bf_skill_profile.clients;

import com.cts.skilltracker.ms_bf_skill_profile.models.UserProfile;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "USER-PROFILE", url="${userprofile.url}")
public interface UserProfileClient {

    @GetMapping("/skill-tracker/users/search-user/name/{name}")
    public ResponseEntity<List<UserProfile>> searchUserByName(@PathVariable String name);

    @GetMapping("/skill-tracker/users/search-user/associateId/{associateId}")
    public ResponseEntity<List<UserProfile>> searchUserByAssociateId(@PathVariable String associateId);

    @GetMapping("/skill-tracker/users/search-user/byAssociates")
    public ResponseEntity<List<UserProfile>> searchUserByAssociates(@RequestParam List<String> associateList);

}
