package com.cts.skilltracker.ms_bf_skill_profile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class MsBfSkillProfileApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsBfSkillProfileApplication.class, args);
	}

}
