package com.cts.skilltracker.ms_bf_skill_profile.exceptionHandler;

import feign.FeignException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request){
        StringBuilder errMsg =new StringBuilder();
        ex.getBindingResult().getAllErrors().forEach(x->errMsg.append(x.getDefaultMessage()));
        CustomExceptionDetail cex = CustomExceptionDetail.builder().code("INVALID_DATA").message(errMsg.toString()).build();
        return new ResponseEntity<>(cex, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value=Exception.class)
    public ResponseEntity<Object> handleAllExceptions(Exception ex){
        CustomExceptionDetail cex = CustomExceptionDetail.builder().code("UNABLE_TO_PROCESS_REQUEST").message("We are currently unable to process your request. Please try later.").build();
        return new ResponseEntity<>(cex, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value=FeignException.class)
    public ResponseEntity<Object> handleFeignClientException(FeignException ex){
        CustomExceptionDetail cex = CustomExceptionDetail.builder().code("NO_USER_FOUND").message("No User found for given criteria").build();
        return new ResponseEntity<>(cex, HttpStatus.BAD_REQUEST);

    }

    @ExceptionHandler(value=HttpClientErrorException.BadRequest.class)
    public ResponseEntity<Object> handleBadRequest(HttpClientErrorException ex){
        CustomExceptionDetail cex = CustomExceptionDetail.builder().code("NO_USER_FOUND").message("No User found for given criteria").build();
        return new ResponseEntity<>(cex, HttpStatus.BAD_REQUEST);

    }

}

