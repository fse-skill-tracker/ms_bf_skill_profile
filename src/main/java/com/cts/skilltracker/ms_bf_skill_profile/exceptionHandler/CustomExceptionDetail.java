package com.cts.skilltracker.ms_bf_skill_profile.exceptionHandler;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomExceptionDetail {
    private String code;
    private String message;
}
