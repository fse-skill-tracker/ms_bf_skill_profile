package com.cts.skilltracker.ms_bf_skill_profile.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class SkillTrackerEvent {

    private String associateId;
    private TechSkills techSkills;
    private SoftSkills softSkills;
}

