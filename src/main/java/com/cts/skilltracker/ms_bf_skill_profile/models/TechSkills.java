package com.cts.skilltracker.ms_bf_skill_profile.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TechSkills {

    private String id;
    private String associateId;
    private int htmlJS;
    private int angular;
    private int react;
    private int spring;
    private int restful;
    private int hibernate;
    private int git;
    private int docker;
    private int jenkins;
    private int aws;
}

