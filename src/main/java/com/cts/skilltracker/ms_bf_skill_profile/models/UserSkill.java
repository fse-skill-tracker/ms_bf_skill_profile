package com.cts.skilltracker.ms_bf_skill_profile.models;

import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserSkill {

    private UserProfile userProfile;
    private TechSkills techSkills;
    private SoftSkills softSkills;
}
