package com.cts.skilltracker.ms_bf_skill_profile.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserProfile {

    private String id;
    private String associateId;
    private String name;
    private String email;
    private String mobile;
}
