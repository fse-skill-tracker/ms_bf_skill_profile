package com.cts.skilltracker.ms_bf_skill_profile.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SoftSkills {

    private String id;
    private String associateId;
    private int spoken;
    private int communication;
    private int aptitude;
}
