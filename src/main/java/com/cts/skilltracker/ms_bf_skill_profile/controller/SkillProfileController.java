package com.cts.skilltracker.ms_bf_skill_profile.controller;

import com.ctc.wstx.util.StringUtil;
import com.cts.skilltracker.ms_bf_skill_profile.clients.UserProfileClient;
import com.cts.skilltracker.ms_bf_skill_profile.clients.UserSkillClient;
import com.cts.skilltracker.ms_bf_skill_profile.models.SkillTrackerEvent;
import com.cts.skilltracker.ms_bf_skill_profile.models.UserProfile;
import com.cts.skilltracker.ms_bf_skill_profile.models.UserSkill;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/skill-tracker/search-profile")
public class SkillProfileController {

    @Autowired
    private UserProfileClient userClient;

    @Autowired
    private UserSkillClient skillClient;

    @GetMapping("/{criteria}/{criteriaValue}")
    public ResponseEntity<?> searchUsers(@PathVariable String criteria,  @PathVariable String criteriaValue){
        HttpStatus status = HttpStatus.OK;
        ResponseEntity responseEntity;
        List<UserSkill> searchedUserSkills = null;
        if(!(StringUtils.equalsIgnoreCase(criteria,"name") || StringUtils.equalsIgnoreCase(criteria,"associateId") || StringUtils.equalsIgnoreCase(criteria,"skill"))){
            status = HttpStatus.BAD_REQUEST;
            return ResponseEntity.status(status).body("Invalid Search Criteria");
        }else{
            System.out.println("valid criteria"+criteria+"::"+criteriaValue);
            if(StringUtils.equalsIgnoreCase(criteria,"name")){
                ResponseEntity<List<UserProfile>> response =  userClient.searchUserByName(criteriaValue);

                if(response.hasBody()){
                    List<UserProfile> users =  response.getBody();

                    if(users != null && users.size() > 0 ){
                        System.out.println("user list size"+users.size());
                        List<String> associateList = users.stream().map(u->u.getAssociateId()).collect(Collectors.toList());
                        if(associateList != null && associateList.size() > 0){
                            ResponseEntity<List<SkillTrackerEvent>> skResponse = skillClient.searchUserByAssociates(associateList);
                            List<SkillTrackerEvent> skEvents = skResponse.getBody();
                            searchedUserSkills = createUserSkillList(associateList, users, skEvents);
                            System.out.println("user skill size"+searchedUserSkills.size());
                        }
                    }
                }
                if(searchedUserSkills == null){
                    status = HttpStatus.BAD_REQUEST;
                }
                return ResponseEntity.status(status).body(searchedUserSkills);

            }else if(StringUtils.equalsIgnoreCase(criteria,"associateId")){
                ResponseEntity<List<UserProfile>> response =  userClient.searchUserByAssociateId(criteriaValue);

                if(response.hasBody()){
                    List<UserProfile> users =  response.getBody();
                    if(users != null && users.size() > 0 ){
                        System.out.println("user list size"+users.size());
                        List<String> associateList = users.stream().map(u->u.getAssociateId()).collect(Collectors.toList());
                        if(associateList != null && associateList.size() > 0){
                            ResponseEntity<List<SkillTrackerEvent>> skResponse = skillClient.searchUserByAssociates(associateList);
                            List<SkillTrackerEvent> skEvents = skResponse.getBody();

                            searchedUserSkills = createUserSkillList(associateList, users, skEvents);
                            System.out.println("user skill size"+searchedUserSkills.size());
                        }
                    }
                }
                if(searchedUserSkills == null){
                    status = HttpStatus.BAD_REQUEST;
                }
                return ResponseEntity.status(status).body(searchedUserSkills);
            }else{
                ResponseEntity<List<SkillTrackerEvent>> skResponse = skillClient.searchUserBySkill(criteriaValue);
                if(skResponse.hasBody()){
                    List<SkillTrackerEvent> skEvents = skResponse.getBody();
                    if(skEvents != null && skEvents.size() > 0 ) {
                        List<String> associateList = skEvents.stream().map(u -> u.getAssociateId()).collect(Collectors.toList());
                        if(associateList != null && associateList.size() > 0){
                            ResponseEntity<List<UserProfile>> usersResponse = userClient.searchUserByAssociates(associateList);
                            List<UserProfile> users = usersResponse.getBody();
                            searchedUserSkills = createUserSkillList(associateList, users, skEvents);
                        }
                    }
                }
            }
            return ResponseEntity.status(status).body(searchedUserSkills);
        }

    }

    public List<UserSkill> createUserSkillList(List<String> associateList, List<UserProfile> users, List<SkillTrackerEvent> skEvents) {
        List<UserSkill> searchedUserSkills = new ArrayList<>();
        associateList.forEach(x -> {
            UserSkill uSkill = new UserSkill();
            Optional<UserProfile> searchedUser = users.stream().filter(y -> StringUtils.equalsIgnoreCase(y.getAssociateId(), x)).findFirst();
            if (searchedUser.isPresent()) {
                uSkill.setUserProfile(searchedUser.get());
                if (skEvents != null && skEvents.size() > 0) {
                    Optional<SkillTrackerEvent> optSkEvent = skEvents.stream().filter(z -> StringUtils.equalsIgnoreCase(z.getAssociateId(), x)).findFirst();
                    if (optSkEvent.isPresent()) {
                        uSkill.setTechSkills(optSkEvent.get().getTechSkills());
                        uSkill.setSoftSkills(optSkEvent.get().getSoftSkills());
                    }
                }
                searchedUserSkills.add(uSkill);
            }

        });
        return searchedUserSkills;

    }

    @GetMapping("/health-check")
    public String healthCheck(){
        return "true";
    }
}
